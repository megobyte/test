// JS
import "./js/";

// SCSS
import "./assets/scss/main.scss";

// CSS (example)
// import './assets/css/main.css'

// Vue.js
window.Vue = require("vue");

// Vue components (for use in html)
Vue.component("example-component", require("./components/Example.vue").default);

// Vue init
const app = new Vue({
  el: "#app",
  data: {
    menu: false,
    scroll: false,
    slides: 5,
    slide: 1,
    hiddenDescription: false,
    sliderHeight: 0
  },
  mounted() {
    const that = this;
    that.scroll = document.documentElement.scrollTop > 0 ? true : false;

    window.addEventListener("scroll", function() {
      that.scroll = document.documentElement.scrollTop > 0 ? true : false;
    });

    setTimeout(function() {
      that.nextSlide();
      that.prevSlide();
    }, 1);
  },
  watch: {
    hiddenDescription() {
      const that = this;
      setTimeout(function() {
        that.nextSlide();
        that.prevSlide();
      }, 1);
    }
  },
  computed: {
    slidesStyle() {
      const that = this;
      const r = {
        width: this.slides * 100 + "%",
        marginLeft: "-" + (this.slide * 100 - 100) + "%",
        height:
          this.$el.querySelectorAll(".slider .slides .slide")[this.slide - 1]
            .offsetHeight +
          3 +
          "px"
      };

      setTimeout(function() {
        const height =
          that.$el.querySelector(".slider .slides").offsetHeight +
          that.$el.querySelector(".slider .arrows").offsetHeight +
          that.slide -
          that.slide +
          80;

        that.$el.querySelector(".slider").style.height = height + 'px';
      }, 600);

      return r;
    },
    slideStyle() {
      const r = {
        width: 100 / this.slides + "%"
      };

      return r;
    }
  },
  methods: {
    prevSlide() {
      if (this.slide === 1) {
        this.slide = this.slides;
      } else {
        this.slide -= 1;
      }
    },

    nextSlide() {
      if (this.slide === this.slides) {
        this.slide = 1;
      } else {
        this.slide += 1;
      }
    }
  }
});
